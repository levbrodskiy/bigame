<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.2" name="tile3" tilewidth="64" tileheight="64" tilecount="20" columns="5">
 <image source="tile3.png" width="320" height="256"/>
 <tile id="1">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
</tileset>
