<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.2" name="tile" tilewidth="64" tileheight="64" tilecount="20" columns="5">
 <image source="tile.png" width="320" height="256"/>
 <tile id="0">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="doorBG" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="keyBG" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="doorBG" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="doorBG" type="bool" value="false"/>
  </properties>
 </tile>
</tileset>
