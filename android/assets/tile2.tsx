<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.2" name="tile2" tilewidth="64" tileheight="64" tilecount="20" columns="5">
 <image source="tile2.png" width="320" height="256"/>
 <tile id="19">
  <properties>
   <property name="doorBG" type="bool" value="false"/>
  </properties>
 </tile>
</tileset>
