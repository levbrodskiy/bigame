package ru.topteam.bigame.controllers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import ru.topteam.bigame.screens.PauseScreen;
import ru.topteam.bigame.ui.Button;

public class GameScreenController {
    private Game game;
    private Button pauseButton;

    public GameScreenController(Game game, Button pauseButton) {
        this.game = game;
        this.pauseButton = pauseButton;
    }

    public void move(Vector3 touchPos) {
        Rectangle touch = new Rectangle();
        touch.set(touchPos.x, touchPos.y, 1, 1);
        if (touch.overlaps(pauseButton.getHitbox())) {
            pauseButton.playClickSound();
            game.setScreen(new PauseScreen(game));
        }

    }

    public void render(SpriteBatch batch) {
        pauseButton.render(batch);
    }
}
