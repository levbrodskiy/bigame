package ru.topteam.bigame.controllers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import ru.topteam.bigame.screens.PauseScreen;
import ru.topteam.bigame.ui.Button;

/**
 * @author egor ivanchin
 */
public class QuestController implements TouchController {
    private Game game;
    private Button goPauseButton;

    public QuestController(Game game, Button goPauseButton) {
        this.game = game;
        this.goPauseButton = goPauseButton;
    }

    public void move(Vector3 vector) {
        Rectangle touch = new Rectangle();
        touch.x = vector.x - RANGE / 2;
        touch.y = Gdx.graphics.getHeight() - vector.y;
        touch.width = RANGE;
        touch.height = RANGE;

        if (touch.overlaps(goPauseButton.getHitbox())) {
            game.setScreen(new PauseScreen(game));
        }
    }

    public void render(SpriteBatch batch) {
        batch.draw(goPauseButton.getButtonTexture(), goPauseButton.getHitbox().x, goPauseButton.getHitbox().y,
                goPauseButton.getHitbox().width, goPauseButton.getHitbox().height);
    }

    public void dispose() {
        goPauseButton.dispose();
    }
}
