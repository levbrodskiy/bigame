package ru.topteam.bigame.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.Game;

import ru.topteam.bigame.screens.AuthorsScreen;
import ru.topteam.bigame.screens.GameScreen;
import ru.topteam.bigame.ui.Button;

/**
 * @author Lev Brodskiy
 * @author Maxim Lebedev
 */
public class MenuController implements TouchController {
    private Button gameBtn;
    private Button authorsBtn;
    private Game game;

    public MenuController(Game game, Button gameBtn, Button authorsBtn) {
        this.game = game;
        this.gameBtn = gameBtn;
        this.authorsBtn = authorsBtn;
    }

    public void move(Vector3 vector) {
        Rectangle touch = new Rectangle();
        touch.x = vector.x - RANGE / 2;
        touch.y = Gdx.graphics.getHeight() - vector.y - RANGE / 2;
        touch.width = RANGE;
        touch.height = RANGE;

        if (touch.overlaps(gameBtn.getHitbox())) {
            gameBtn.playClickSound();
            GameScreen.restart(game);
            game.setScreen(GameScreen.getInstance(game));
        }

        if (touch.overlaps(authorsBtn.getHitbox())) {
            gameBtn.playClickSound();
            game.setScreen(new AuthorsScreen(game));
        }
    }

    public void render(SpriteBatch batch) {
        batch.draw(gameBtn.getButtonTexture(), gameBtn.getHitbox().x, gameBtn.getHitbox().y,
                gameBtn.getHitbox().width, gameBtn.getHitbox().height);

        batch.draw(authorsBtn.getButtonTexture(), authorsBtn.getHitbox().x, authorsBtn.getHitbox().y,
                authorsBtn.getHitbox().width, authorsBtn.getHitbox().height);
    }

    public void dispose() {
        gameBtn.dispose();
        authorsBtn.dispose();
    }
}
