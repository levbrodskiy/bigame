package ru.topteam.bigame.controllers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import ru.topteam.bigame.screens.MenuScreen;
import ru.topteam.bigame.ui.Button;

public class GameWinController implements TouchController{
    private Game game;
    private Button goMenuButton;

    public GameWinController(Game game, Button goMenuButton){
        this.game = game;
        this.goMenuButton = goMenuButton;
    }

    public void move(Vector3 vector){
        Rectangle touch = new Rectangle();
        touch.x = vector.x;
        touch.y = Gdx.graphics.getHeight() - vector.y;
        touch.width = RANGE;
        touch.height = RANGE;

        if (touch.overlaps(goMenuButton.getHitbox())){
            goMenuButton.playClickSound();
            game.setScreen(new MenuScreen(game));
        }
    }

    public void render(SpriteBatch batch){
        batch.draw(goMenuButton.getButtonTexture(), goMenuButton.getHitbox().x, goMenuButton.getHitbox().y,
                goMenuButton.getHitbox().width, goMenuButton.getHitbox().height);
    }

    public void dispose(){
        goMenuButton.dispose();
    }
}
