package ru.topteam.bigame.controllers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import ru.topteam.bigame.screens.MenuScreen;
import ru.topteam.bigame.ui.Button;

/**
 * @author Lev Brodskiy
 */
public class AuthorsController implements TouchController {
    private Game game;
    private Button exitBtn;

    public AuthorsController(Game game, Button exitBtn) {
        this.game = game;
        this.exitBtn = exitBtn;
    }

    public void move(Vector3 vector){
        Rectangle touch = new Rectangle();
        touch.x = vector.x;
        touch.y = Gdx.graphics.getHeight() - vector.y;
        touch.width = RANGE;
        touch.height = RANGE;

        if (touch.overlaps(exitBtn.getHitbox())){
            exitBtn.playClickSound();
            game.setScreen(new MenuScreen(game));
        }
    }

    public void render(SpriteBatch batch){
        batch.draw(exitBtn.getButtonTexture(), exitBtn.getHitbox().x, exitBtn.getHitbox().y,
                exitBtn.getHitbox().width, exitBtn.getHitbox().height);
    }

    public void dispose(){
        exitBtn.dispose();
    }
}
