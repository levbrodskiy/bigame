package ru.topteam.bigame.controllers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import ru.topteam.bigame.screens.GameScreen;
import ru.topteam.bigame.screens.MenuScreen;
import ru.topteam.bigame.ui.Button;

/**
 * @author Lev Brodskiy
 */
public class PauseController implements TouchController {
    private Game game;
    private Button backToGameButton;
    private Button backToMenuButton;

    public PauseController(Game game, Button backToGameButton, Button backToMenuButton) {
        this.game = game;
        this.backToGameButton = backToGameButton;
        this.backToMenuButton = backToMenuButton;
    }

    public void move(Vector3 vector) {
        Rectangle touch = new Rectangle();
        touch.x = vector.x - RANGE / 2;
        touch.y = Gdx.graphics.getHeight() - vector.y;
        touch.width = RANGE;
        touch.height = RANGE;

        if (touch.overlaps(backToGameButton.getHitbox())) {
            backToGameButton.playClickSound();
            game.setScreen(GameScreen.getInstance(game));
        }

        if (touch.overlaps(backToMenuButton.getHitbox())) {
            backToMenuButton.playClickSound();
            game.setScreen(new MenuScreen(game));
        }
    }

    public void render(SpriteBatch batch) {
        backToGameButton.render(batch);
        backToMenuButton.render(batch);
    }

    public void dispose() {
        backToMenuButton.dispose();
        backToGameButton.dispose();
    }
}
