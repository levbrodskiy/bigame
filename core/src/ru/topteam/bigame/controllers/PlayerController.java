package ru.topteam.bigame.controllers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import ru.topteam.bigame.persons.Player;
import ru.topteam.bigame.ui.Button;


/**
 * @author Lev Brodskiy
 */
public class PlayerController implements TouchController {
    private Button attack;
    private Button up;
    private Button down;
    private Button left;
    private Button right;
    private Player player;

    public PlayerController(Button attack, Button up, Button down, Button left, Button right, Player player) {
        this.attack = attack;
        this.up = up;
        this.down = down;
        this.left = left;
        this.right = right;
        this.player = player;
    }

    public void doMove(Vector3 vector, float dt) {
        Rectangle touch = new Rectangle();
        touch.x = vector.x - RANGE / 2;
        touch.y = vector.y - RANGE / 2;
        touch.width = RANGE;
        touch.height = RANGE;

        if (touch.overlaps(up.getHitbox())) {
            player.moveUp(dt);
        }

        if (touch.overlaps(down.getHitbox())) {
            player.moveDown(dt);
        }

        if (touch.overlaps(right.getHitbox())) {
            player.moveRight(dt);
        }

        if (touch.overlaps(left.getHitbox())) {
            player.moveLeft(dt);
        }

        if (touch.overlaps(attack.getHitbox())) {
            player.attack();
        }
    }

    public void render(SpriteBatch batch) {
        up.render(batch);
        down.render(batch);
        left.render(batch);
        right.render(batch);
    }

    public void dispose() {
        up.dispose();
        down.dispose();
        right.dispose();
        left.dispose();
    }

    public void stopMove(float dt) {
        player.stopMove(dt);
    }
}
