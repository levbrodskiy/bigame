package ru.topteam.bigame.future;

/**
 * @author Lev Brodskiy
 */
public enum Orientation {
    LEFT(90), RIGHT(-90), UP(0), DOWN(180);

    private float rotation;

    Orientation(float rotation) {
        this.rotation = rotation;
    }

    public float getRotation(){
        return rotation;
    }
}
