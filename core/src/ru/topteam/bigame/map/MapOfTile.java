package ru.topteam.bigame.map;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

/**
 * author Anton
 */

public class MapOfTile {

    private TiledMap map;
    private TiledMapRenderer renderer;

    public void mapLoad(){
        map = new TmxMapLoader().load("map.tmx");
        renderer = new OrthogonalTiledMapRenderer(map);
    }

    public void render(OrthographicCamera camera) {
        renderer.render();
        renderer.setView(camera);
    }

    public TiledMapTileLayer getTileLayers(int index){
        return (TiledMapTileLayer) map.getLayers().get(index);
    }

}
