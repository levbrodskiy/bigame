package ru.topteam.bigame.map;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;

import ru.topteam.bigame.persons.Player;

/**
 * author Anton
 */

public class PlayerCollision {

    private float tileWidth;
    private float tileHeight;
    private TiledMapTileLayer bgLayer;
    private MapOfTile map;
    private Player player;
    private TiledMapTileLayer keyLayer;
    private MapInteraction mapInteraction;


    public PlayerCollision(MapOfTile map, Player player,MapInteraction mapInteraction) {
        this.mapInteraction = mapInteraction;
        this.player = player;
        this.map = map;
        bgLayer = map.getTileLayers(0);
        tileWidth = map.getTileLayers(0).getTileWidth();
        tileHeight = map.getTileLayers(0).getTileHeight();
    }

    public void wallCollision(Rectangle playerHibox) {


        boolean collisionY = false;
        boolean collisionX = false;
        boolean collisionXY = false;
        boolean collisionYX = false;


        //чек лево/право
        collisionX = bgLayer.getCell((int) ((playerHibox.getX()) / tileWidth) + 1, (int) ((playerHibox.getY()) / tileHeight) + 1).getTile().getProperties().containsKey("blocked");
        collisionYX = bgLayer.getCell((int) ((playerHibox.getX()) / tileWidth) + 1, (int) ((playerHibox.getY()) / tileHeight) + 1).getTile().getProperties().containsKey("blocked");

        if (collisionX) {
            if (collisionYX) {
                player.currentSprite.setY(player.oldPosition.getY());
                player.currentPosition.setY(player.oldPosition.getY());
            }
            player.currentPosition.setX(player.oldPosition.getX());
            player.currentPosition.setX(player.oldPosition.getX());
        }

        //чек низ/верх
        collisionY = bgLayer.getCell((int) (player.currentPosition.getX() / tileWidth) + 1, (int) ((player.currentPosition.getY()) / tileHeight) + 1).getTile().getProperties().containsKey("blocked");
        collisionXY = bgLayer.getCell((int) (player.currentPosition.getX() / tileWidth) + 2, (int) ((player.currentPosition.getY()) / tileHeight) ).getTile().getProperties().containsKey("blocked");

        if (collisionY) {
            if (collisionXY) {
                player.currentSprite.setY(player.oldPosition.getY());
                player.currentPosition.setY(player.oldPosition.getY());
            }
            player.currentSprite.setY(player.oldPosition.getY());
            player.currentPosition.setY(player.oldPosition.getY());
        }







    }

    public void doorCollision(TiledMapTileLayer keyLayer) {
        boolean collisionDY = false;
        collisionDY = bgLayer.getCell((int) (player.currentPosition.getX() / tileWidth) + 1, (int) ((player.currentPosition.getY()) / tileHeight) + 1).getTile().getProperties().containsKey("doorBG");
        if (collisionDY&&(!keyLayer.isVisible())) {
            mapInteraction.openDoor(player);
        }
    }
}
