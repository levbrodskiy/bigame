package ru.topteam.bigame.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;

import ru.topteam.bigame.persons.Player;

/**
 * author Anton
 */

public class MapInteraction {

    private MapOfTile map;
    private TiledMapTileLayer bgLayer;
    private float tileWidth;
    private float tileHeight;
    public boolean doorOpen;
    private Music getKeyMusic;
    private int limiter;

    public void setBgLayer(TiledMapTileLayer bgLayer) {
        this.bgLayer = bgLayer;
    }

    public MapInteraction(MapOfTile map) {
        this.map = map;
        bgLayer = map.getTileLayers(0);
        tileWidth = map.getTileLayers(1).getTileWidth();
        tileHeight = map.getTileLayers(1).getTileHeight();
        limiter=0;
    }

    public boolean isKeyBeforePlayer(Rectangle playerHitbox) {
        if ((bgLayer.getCell((int) ((playerHitbox.getX()) / tileWidth) + 1, (int) ((playerHitbox.getY()) / tileHeight) + 1).getTile().getProperties().containsKey("keyBG")) || (bgLayer.getCell((int) (playerHitbox.getX() / tileWidth) + 1, (int) ((playerHitbox.getY()) / tileHeight) + 1).getTile().getProperties().containsKey("keyBG"))) {
            return true;
        }
        return false;
    }

    public void getKey(TiledMapTileLayer keyLayer) {
        if (limiter==0) {
            getKeyMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/key_up.mp3"));
            getKeyMusic.setVolume(10);
            getKeyMusic.play();
            keyLayer.setVisible(false);
        }
        limiter++;
    }

    public boolean isDoorBeforePlayer(Rectangle playerHitbox, TiledMapTileLayer keyLayer) {
        if (((bgLayer.getCell((int) ((playerHitbox.getX()) / tileWidth) + 1, (int) ((playerHitbox.getY()) / tileHeight) + 1).getTile().getProperties().containsKey("doorBG")))  && (!keyLayer.isVisible())) {
            return true;
        }
        return false;
    }

    public void openDoor(Player player) {
        TiledMapTile tile = bgLayer.getCell(0, 33).getTile();
        bgLayer.getCell((int) (player.currentPosition.getX() / tileWidth) + 1, (int) ((player.currentPosition.getY()) / tileHeight) + 1).setTile(tile);
        getKeyMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/door_opening.mp3"));
        getKeyMusic.setVolume(10);
        getKeyMusic.play();
        doorOpen = true;


    }

    public boolean getDoorOpen(){
        return doorOpen;
    }



}
