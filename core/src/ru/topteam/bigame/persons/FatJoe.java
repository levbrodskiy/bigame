package ru.topteam.bigame.persons;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import ru.topteam.bigame.animation.Animation;
import ru.topteam.bigame.utilities.Point2D;

/**
 * @author Lev Brodskiy
 */
public class FatJoe {
    private Point2D currentPosition;
    private Animation animation;
    private Sprite currentSprite;
    private Rectangle hitbox;

    public FatJoe(Rectangle hitbox) {
        this.currentPosition = new Point2D(hitbox.getX(), hitbox.getY());
        this.hitbox = hitbox;
        animation= new Animation(new Texture("animationSleepingBoss.png"), 9, 1f);
        currentSprite = new Sprite(animation.getFrame());
        currentSprite.setPosition(currentPosition.getX(), currentPosition.getY());
        currentSprite.setSize(hitbox.getWidth(), hitbox.getHeight());
    }

    public void render(SpriteBatch batch, float dt){
        animation.update(dt);
        currentSprite = new Sprite(animation.getFrame());
        currentSprite.setPosition(currentPosition.getX(), currentPosition.getY());
        currentSprite.draw(batch);
    }

    public void dispose(){

    }

    public Rectangle getHitbox() {
        return hitbox;
    }
}
