package ru.topteam.bigame.persons;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import ru.topteam.bigame.utilities.Point2D;

public abstract class BasePerson {
    private Point2D currentPosition;
    private Rectangle hitbox;
    private Sprite currentSprite;

    protected BasePerson(Point2D currentPosition, Rectangle hitbox, Sprite currentSprite) {
        this.currentPosition = currentPosition;
        this.hitbox = hitbox;
        this.currentSprite = currentSprite;
    }

    public abstract void render(SpriteBatch batch, float dt);

    public abstract void dispose();

    protected Point2D getCurrentPosition() {
        return currentPosition;
    }

    protected void setCurrentPosition(Point2D currentPosition) {
        this.currentPosition = currentPosition;
    }

    protected Rectangle getHitbox() {
        return hitbox;
    }

    protected void setHitbox(Rectangle hitbox) {
        this.hitbox = hitbox;
    }

    protected Sprite getCurrentSprite() {
        return currentSprite;
    }

    protected void setCurrentSprite(Sprite currentSprite) {
        this.currentSprite = currentSprite;
    }
}
