package ru.topteam.bigame.persons;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import ru.topteam.bigame.utilities.Point2D;
import ru.topteam.bigame.animation.Animation;

/**
 * @author Lev Brodskiy
 */
public class Player  {
    public Point2D currentPosition;
    public Sprite currentSprite;
    public Point2D oldPosition;
    private Rectangle hitbox;
    private float rotationPosition;
    private float speed;
    private Animation walkingAnimation;
    private Sprite standSprite;

    public Player(Rectangle playerHitbox, float speed) {
        this.hitbox = playerHitbox;
        this.speed = speed;
        currentPosition = new Point2D(playerHitbox.x, playerHitbox.y);

        walkingAnimation= new Animation(new Texture("walkAnimation.png"), 2, 0.5f);

        standSprite = new Sprite(new Texture("mainPersonStand.png"));
        standSprite.setPosition(currentPosition.getX() , currentPosition.getY());
        standSprite.setSize(128, 128);

        currentSprite = new Sprite(standSprite);
        currentSprite.setPosition(currentPosition.getX(), currentPosition.getY() );
        currentSprite.setSize(128, 128);
    }

    public void mapInteractionCheck(SpriteBatch batch) {

    }


    private void setPosition(){
        oldPosition = new Point2D(currentPosition);
    }

    public void moveUp(float dt){
        setPosition();
        currentPosition.addY(speed);
        moveAnimate(dt, 0);
    }

    public void moveDown(float dt){
        setPosition();
        currentPosition.addY(-speed);
        moveAnimate(dt, 180);
    }

    public void moveRight(float dt){
        setPosition();
        currentPosition.addX(speed);
        moveAnimate(dt, -90);
    }

    public void moveLeft(float dt){
        setPosition();
        currentPosition.addX(-speed);
        moveAnimate(dt, 90);
    }

    public void stopMove(float dt){
        walkingAnimation.update(dt);
        currentSprite = standSprite;
        currentSprite.setRotation(rotationPosition);
    }

    private void moveAnimate(float dt, float rotationPosition){
        walkingAnimation.update(dt);
        currentSprite = new Sprite(walkingAnimation.getFrame());
        currentSprite.setRotation(rotationPosition);
        this.rotationPosition = rotationPosition;
    }

    public void attack(){

    }

    public void render(SpriteBatch batch){
        currentSprite.setPosition(currentPosition.getX() , currentPosition.getY() );
        hitbox.setX(currentPosition.getX());
        hitbox.setY(currentPosition.getY());
        currentSprite.draw(batch);
    }

    public Rectangle getHitbox() {
        return hitbox;
    }
}
