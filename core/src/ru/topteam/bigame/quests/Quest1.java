package ru.topteam.bigame.quests;

import com.badlogic.gdx.Game;

import com.badlogic.gdx.math.Vector3;

import ru.topteam.bigame.map.MapInteraction;
import ru.topteam.bigame.persons.FatJoe;
import ru.topteam.bigame.persons.Player;
import ru.topteam.bigame.screens.GameLoseScreen;
import ru.topteam.bigame.screens.GameWinScreen;

/**
 * author Anton
 */

public class Quest1 {
    private Player player;
    private FatJoe fatJoe;
    private Game game;
    private MapInteraction mapInteraction;
    private QuestStatus questStatus;
    private QuestNotification questNotification;

    public Quest1(Player player, FatJoe fatJoe, Game game, MapInteraction mapInteraction,QuestNotification questNotification) {
        this.mapInteraction = mapInteraction;
        this.game = game;
        this.player = player;
        this.fatJoe = fatJoe;
        this.questNotification = questNotification;
    }

    public void render(Vector3 vector3) {
        questNotification.render(vector3);
        if ((player.getHitbox().overlaps(fatJoe.getHitbox()))) {
            questStatus = QuestStatus.FAILED;
            game.setScreen(new GameLoseScreen(game));
        }
        if (mapInteraction.getDoorOpen()){
            questStatus = QuestStatus.COMPLETED;
            game.setScreen(new GameWinScreen(game));
        }
        if (questStatus != QuestStatus.INPROCESS) {
            return;
        }
    }


}
