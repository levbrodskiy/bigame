package ru.topteam.bigame.quests;

import com.badlogic.gdx.Game;

import ru.topteam.bigame.map.MapInteraction;
import ru.topteam.bigame.persons.FatJoe;
import ru.topteam.bigame.persons.Player;

public abstract class BaseQuest {
    protected Player player;
    protected FatJoe fatJoe;
    protected Game game;
    protected MapInteraction mapInteraction;
    protected QuestStatus questStatus;
    protected QuestNotification questNotification;
}
