package ru.topteam.bigame.quests;

import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;


import ru.topteam.bigame.ui.Button;

import static jdk.nashorn.internal.runtime.regexp.joni.constants.AsmConstants.RANGE;

/**
 * author Anton
 */
public class QuestNotification {

    private Button acceptButton;
    private boolean show;
    private Sprite questSprite;
    private SpriteBatch spriteBatch;
    private QuestStatus questStatus;
    private Music message;

    public QuestNotification(Button acceptButton, Texture questContent, QuestStatus questStatus, Music music) {
        this.message = music;
        message.setVolume(10);
        message.play();
        this.questStatus = questStatus;
        this.acceptButton = acceptButton;
        show = true;
        questSprite = new Sprite(questContent);
        spriteBatch = new SpriteBatch();
        questSprite.setSize((Gdx.graphics.getWidth() - (Gdx.graphics.getWidth() / 3)), (Gdx.graphics.getHeight()));
        questSprite.setX((Gdx.graphics.getWidth() / 3) - (Gdx.graphics.getWidth() / 6));
        questSprite.setY((Gdx.graphics.getHeight() / 10));
        acceptButton.setButtonX(questSprite.getX() + questSprite.getWidth() - acceptButton.getHitbox().width);
        acceptButton.setButtonY(questSprite.getY() - 80);
    }

    public void render(Vector3 vector) {
        if (!show) {
            return;
        }
        Rectangle touch = new Rectangle();
        touch.x = vector.x + RANGE / 2;
        touch.y = Gdx.graphics.getHeight() - vector.y + RANGE / 2;
        touch.width = RANGE;
        touch.height = RANGE;


        if (touch.overlaps(acceptButton.getHitbox())) {
            acceptButton.playClickSound();
            message.stop();
            message.dispose();
            questStatus = QuestStatus.INPROCESS;
            show = false;
        }
    }

    public void render() {
        if (show) {
            spriteBatch.begin();
            acceptButton.render(spriteBatch);
            questSprite.draw(spriteBatch);
            spriteBatch.end();
        }
    }
}
