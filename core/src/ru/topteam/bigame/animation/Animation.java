package ru.topteam.bigame.animation;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * @author Egor Ivanchin
 */
public class Animation{
    private Array<TextureRegion> frames;
    private float maxFrameTime ;
    private  float currentFrameTime;
    private int frameCount;
    private int frameIndex;
    public Animation(Texture region, int frameCount, float cycleTime){
        frames = new Array<>();
        int frameWidth = region.getWidth() / frameCount;
        for(int i = 0; i < frameCount;i++){
            frames.add(new TextureRegion(region,i*frameWidth,0,frameWidth,region.getHeight()));
        }
        this.frameCount = frameCount;
        maxFrameTime = cycleTime / frameCount;
        frameIndex = 0;
    }

    public void update(float dt){
        currentFrameTime+=dt;
        if(currentFrameTime>maxFrameTime){
            frameIndex++;
            currentFrameTime = 0;
        }
        if ( frameIndex>= frameCount)
            frameIndex = 0;
    }
    public TextureRegion getFrame(){
        return frames.get(frameIndex);

    }
}
