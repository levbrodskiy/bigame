package ru.topteam.bigame.gameitems;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

import ru.topteam.bigame.utilities.Point2D;

/**
 * @author Lev Brodskiy
 */
public class Key implements Item {
    private Texture texture;
    private Point2D position;
    private Rectangle hitbox;
    private float weight;
    private float height;

    public Key(Texture texture, Point2D position, Rectangle hitbox, float weight, float height) {
        this.texture = texture;
        this.position = position;
        this.hitbox = hitbox;
        this.weight = weight;
        this.height = height;
    }
}
