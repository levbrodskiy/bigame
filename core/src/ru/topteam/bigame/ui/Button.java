package ru.topteam.bigame.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * @author Lev Brodskiy
 */
public class Button {
    private Texture buttonTexture;
    private Rectangle hitbox;
    private Sound sound;

    public Button(Texture buttonTexture, Rectangle hitbox) {
        this.buttonTexture = buttonTexture;
        this.hitbox = hitbox;
        sound = Gdx.audio.newSound(Gdx.files.internal("sounds/click.mp3"));
    }

    public void playClickSound() {
        sound.play();
    }

    public void render(SpriteBatch batch) {
        batch.draw(buttonTexture, hitbox.getX(), hitbox.getY(), hitbox.width, hitbox.height);
    }

    public void dispose() {
        buttonTexture.dispose();
    }

    public Texture getButtonTexture() {
        return buttonTexture;
    }

    public void setButtonTexture(Texture buttonTexture) {
        this.buttonTexture = buttonTexture;
    }

    public Rectangle getHitbox() {
        return hitbox;
    }

    public void setButtonX(float x){
        this.getHitbox().x=x;
    }

    public void setButtonWidth(float width){
     hitbox.width =width;
    }

    public void setButtonHeight(float height){
        hitbox.height = height;
    }

    public void setButtonY(float y){
        this.getHitbox().y=y;
    }

    public void setHitbox(Rectangle hitbox) {
        this.hitbox = hitbox;
    }
}
