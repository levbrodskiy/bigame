package ru.topteam.bigame.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import ru.topteam.bigame.controllers.GameLoseController;
import ru.topteam.bigame.ui.Button;

/**
 * @author egor ivanchin
 */
public class GameLoseScreen implements Screen {
    private SpriteBatch batch;
    private Game game;
    private GameLoseController gameLoseController;
    private Music loseMusic;
    private int widthTexture = 512;
    private int heightTexture = 135;
    private Sprite bgSprite;

    public GameLoseScreen(Game game) {
        this.game = game;
    }

    @Override
    public void show() {
        bgSprite = new Sprite(new Texture(Gdx.files.internal("youLoseScreen.png")));
        bgSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        Texture buttonImage = new Texture(Gdx.files.internal("back_to_menu_button.png"));
        Rectangle goMenuHitbox = new Rectangle(Gdx.graphics.getWidth() / 2 - widthTexture / 2, Gdx.graphics.getHeight() / 2 - heightTexture / 2, widthTexture, heightTexture);
        Button goMenuButton = new Button(buttonImage, goMenuHitbox);
        gameLoseController = new GameLoseController(game, goMenuButton);
        goMenuHitbox.setY(goMenuHitbox.y - 240);
        batch = new SpriteBatch();
        loseMusic = Gdx.audio.newMusic(Gdx.files.internal("music/lose.mp3"));
        loseMusic.setVolume(10);
        loseMusic.play();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (Gdx.input.isTouched()) {
            gameLoseController.move(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
        }

        batch.begin();
        bgSprite.draw(batch);
        gameLoseController.render(batch);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
        loseMusic.pause();
    }

    @Override
    public void resume() {
        loseMusic.play();
    }

    @Override
    public void hide() {
        loseMusic.pause();
    }

    @Override
    public void dispose() {
        batch.dispose();
        gameLoseController.dispose();
        loseMusic.dispose();
    }
}
