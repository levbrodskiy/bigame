package ru.topteam.bigame.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import ru.topteam.bigame.controllers.PauseController;
import ru.topteam.bigame.ui.Button;

/**
 * @author Maxim Lebedev
 * @author Lev Brodskiy
 */
public class PauseScreen implements Screen {

    private SpriteBatch batch;
    private Game game;
    private PauseController pauseController;

    private int widthButton = 512;
    private int heightButton = 136;

    private OrthographicCamera camera;
    private Sprite bgSprite;

    public PauseScreen(Game game) {
        this.game = game;

    }

    @Override
    public void show() {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, 0);

        Texture buttonImageGame = new Texture(Gdx.files.internal("back_to_game_button.png"));
        Texture buttonImageMenu = new Texture(Gdx.files.internal("back_to_menu_button.png"));

        bgSprite = new Sprite(new Texture(Gdx.files.internal("bg.jpg")));
        bgSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        Rectangle buttonGame = new Rectangle(Gdx.graphics.getWidth() / 2 - widthButton / 2, Gdx.graphics.getHeight() / 2, widthButton, heightButton);
        Rectangle buttonMenu = new Rectangle(Gdx.graphics.getWidth() / 2 - widthButton / 2, Gdx.graphics.getHeight() / 2 - 150, widthButton, heightButton);
        Button buttonBackGame = new Button(buttonImageGame, buttonGame);
        Button buttonBackMenu = new Button(buttonImageMenu, buttonMenu);

        pauseController = new PauseController(game, buttonBackGame, buttonBackMenu);
        batch = new SpriteBatch();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (Gdx.input.isTouched()) {
            pauseController.move(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
        }

        camera.update();

        batch.begin();
        bgSprite.draw(batch);
        batch.setProjectionMatrix(camera.combined);
        pauseController.render(batch);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        pauseController.dispose();
    }
}
