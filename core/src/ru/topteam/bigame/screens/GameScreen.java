package ru.topteam.bigame.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import ru.topteam.bigame.controllers.GameScreenController;
import ru.topteam.bigame.map.MapInteraction;
import ru.topteam.bigame.map.MapOfTile;
import ru.topteam.bigame.map.PlayerCollision;
import ru.topteam.bigame.persons.FatJoe;
import ru.topteam.bigame.persons.Player;
import ru.topteam.bigame.controllers.PlayerController;
import ru.topteam.bigame.quests.Quest1;
import ru.topteam.bigame.quests.QuestNotification;
import ru.topteam.bigame.quests.QuestStatus;
import ru.topteam.bigame.ui.Button;

public class GameScreen implements Screen {
    private Music music;
    private SpriteBatch batch;
    private Rectangle playerHitbox;
    private OrthographicCamera camera;
    private Quest1 quest1;
    private Texture buttonImage;
    private Rectangle buttonLeft;
    private Rectangle buttonUp;
    private Rectangle buttonRight;
    private Rectangle buttonDown;
    private Rectangle buttonAttack;
    private Player player;
    private FatJoe fatJoe;
    private QuestNotification questNotification;
    private PlayerController controller;
    private MapOfTile map;
    private MapInteraction mapInteraction;
    private PlayerCollision playerCollision;
    private GameScreenController gameScreenController;
    private Game game;
    public static final int WIDTH = 800;
    public static final int HEIGHT = 400;
    private static GameScreen gameScreen;
    private Rectangle pauseButtonHitbox;

    private GameScreen(Game game) {
        this.game = game;
        music = Gdx.audio.newMusic(Gdx.files.internal("music/test_music.mp3"));
        buttonImage = new Texture(Gdx.files.internal("buttonControll.png"));
        camera = new OrthographicCamera();

        pauseButtonHitbox = new Rectangle();
        pauseButtonHitbox.setHeight(64);
        pauseButtonHitbox.setWidth(64);
        pauseButtonHitbox.setX(Gdx.graphics.getWidth() - pauseButtonHitbox.getWidth());
        pauseButtonHitbox.setY(Gdx.graphics.getHeight() - pauseButtonHitbox.getHeight());
        Texture pauseButtonTexture = new Texture(Gdx.files.internal("pause_button.png"));
        Button pauseButton = new Button(pauseButtonTexture, pauseButtonHitbox);
        gameScreenController = new GameScreenController(game, pauseButton);
        playerHitbox = new Rectangle(1300, 550, 64, 64);

        map = new MapOfTile();

        map.mapLoad();

        player = new Player(playerHitbox, 3);

        mapInteraction = new MapInteraction(map);

        playerCollision = new PlayerCollision(map, player, mapInteraction);

        fatJoe = new FatJoe(new Rectangle(1301, 1200, 207, 330));
        buttonUp = new Rectangle(camera.position.x - 260, camera.position.y - 110, 64, 64);
        buttonDown = new Rectangle(camera.position.x - 260, camera.position.y - 180, 64, 64);
        buttonRight = new Rectangle(camera.position.x - 190, camera.position.y - 145, 64, 64);
        buttonLeft = new Rectangle(camera.position.x - 330, camera.position.y - 145, 64, 64);
        buttonAttack = new Rectangle(400, 400, 64, 64);

        controller = new PlayerController(new Button(buttonImage, buttonAttack),
                new Button(buttonImage, buttonUp),
                new Button(buttonImage, buttonDown),
                new Button(buttonImage, buttonLeft),
                new Button(buttonImage, buttonRight), player);

        Button acceptButton = new Button(new Texture("acceptButton.png"), new Rectangle(0, 0, 600, 200));
        Music questMessage = Gdx.audio.newMusic(Gdx.files.internal("music/firstQuestMessage.wav"));
        questNotification = new QuestNotification(acceptButton, new Texture("questNotification.png"), QuestStatus.NOTTAKE, questMessage);

        quest1 = new Quest1(player, fatJoe, game, mapInteraction, questNotification);

        batch = new SpriteBatch();
    }

    public static GameScreen getInstance(Game game) {
        if (gameScreen == null) {
            gameScreen = new GameScreen(game);
        }
        return gameScreen;
    }

    @Override
    public void show() {
        music.setLooping(true);
        music.setVolume(0.01f);
        music.play();
    }

    @Override
    public void render(float dt) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        map.render(camera);

        camera.setToOrtho(false, new Float(Gdx.graphics.getWidth()/2.5), new Float(Gdx.graphics.getHeight()/2.5));
        camera.position.set(player.currentPosition.getX(), player.currentPosition.getY(), 0);
        camera.update();

        buttonUp.setX(camera.position.x - 260);
        buttonUp.setY(camera.position.y - 110);
        buttonDown.setX(camera.position.x - 260);
        buttonDown.setY(camera.position.y - 180);
        buttonLeft.setX(camera.position.x - 330);
        buttonLeft.setY(camera.position.y - 145);
        buttonRight.setX(camera.position.x - 190);
        buttonRight.setY(camera.position.y - 145);

        pauseButtonHitbox.setX((camera.position.x + camera.viewportWidth / 2) - pauseButtonHitbox.getWidth());
        pauseButtonHitbox.setY((camera.position.y + camera.viewportHeight /2) - pauseButtonHitbox.getHeight());

        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        fatJoe.render(batch, Gdx.graphics.getDeltaTime());
        player.render(batch);
        mapInteractionCheck();
        fatJoe.render(batch, Gdx.graphics.getDeltaTime());
        gameScreenController.render(batch);
        controller.render(batch);
        batch.end();

        questNotification.render();

        if (Gdx.input.isTouched()) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            quest1.render(touchPos);
            camera.unproject(touchPos);
            controller.doMove(touchPos, Gdx.graphics.getDeltaTime());
            gameScreenController.move(touchPos);
        } else {
            controller.stopMove(Gdx.graphics.getDeltaTime());
        }
    }

    private void mapInteractionCheck() {
        playerCollision.doorCollision(map.getTileLayers(1));
        if (mapInteraction.isKeyBeforePlayer(playerHitbox)) {
            mapInteraction.getKey(map.getTileLayers(1));
        }

        playerCollision.doorCollision(map.getTileLayers(1));
        if (mapInteraction.isDoorBeforePlayer(playerHitbox, map.getTileLayers(1))) {
            mapInteraction.openDoor(player);
        }
        playerCollision.wallCollision(playerHitbox);
    }

    public static void restart(Game game) {
        gameScreen = new GameScreen(game);
    }

    @Override
    public void dispose() {
        controller.dispose();
        batch.dispose();
        fatJoe.dispose();
        music.dispose();
    }

    @Override
    public void resize(int width, int height) {
        camera.setToOrtho(false, width, height);
        camera.update();
    }

    @Override
    public void pause() {
        music.pause();
    }

    @Override
    public void resume() {
        music.play();
    }

    @Override
    public void hide() {
        music.pause();
    }
}
