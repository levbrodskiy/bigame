package ru.topteam.bigame.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import ru.topteam.bigame.controllers.MenuController;
import ru.topteam.bigame.ui.Button;

/**
 * @author Maxim Lebedev
 * @author Lev Brodskiy
 */
public class MenuScreen implements Screen {

    private SpriteBatch batch;
    private MenuController menuController;
    private Game game;
    private OrthographicCamera camera;

    private int widthButton = 512;
    private int heightButton = 136;
    private Sprite bgSprite;

    public MenuScreen(Game game) {
        this.game = game;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, 0);

        Texture buttonImageGame = new Texture(Gdx.files.internal("play_game_button.png"));
        Texture buttonImageAuthors = new Texture(Gdx.files.internal("authors_button.png"));

        bgSprite = new Sprite(new Texture(Gdx.files.internal("mainBg.png")));
        bgSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        Rectangle buttonGame = new Rectangle(Gdx.graphics.getWidth() / 2 - widthButton / 2, Gdx.graphics.getHeight() / 2, widthButton, heightButton);
        Rectangle buttonAuthors = new Rectangle(Gdx.graphics.getWidth() / 2 - widthButton / 2, Gdx.graphics.getHeight() / 2 - 150, widthButton, heightButton);
        Button buttonStartGame = new Button(buttonImageGame, buttonGame);
        Button buttonStartAuthors = new Button(buttonImageAuthors, buttonAuthors);

        menuController = new MenuController(game, buttonStartGame, buttonStartAuthors);
        batch = new SpriteBatch();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (Gdx.input.isTouched()) {
            menuController.move(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
        }

        camera.update();

        batch.begin();
        bgSprite.draw(batch);
        batch.setProjectionMatrix(camera.combined);
        menuController.render(batch);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        menuController.dispose();
    }
}
