package ru.topteam.bigame.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import ru.topteam.bigame.controllers.AuthorsController;
import ru.topteam.bigame.ui.Button;

/**
 * @author Maxim Lebedev
 */
public class AuthorsScreen implements Screen {

    private SpriteBatch batch;
    private Game game;
    private AuthorsController authorsController;

    private int widthButton = 128;
    private int heightButton = 128;

    private OrthographicCamera camera;
    private Sprite bgSprite;

    public AuthorsScreen(Game game) {
        this.game = game;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, 0);

        Texture buttonImage = new Texture(Gdx.files.internal("closingButton.png"));
        bgSprite = new Sprite(new Texture(Gdx.files.internal("authorsBg.png")));
        bgSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Rectangle buttonExit = new Rectangle(Gdx.graphics.getWidth() - widthButton, Gdx.graphics.getHeight() - heightButton, widthButton, heightButton);
        Button buttonStartExit = new Button(buttonImage, buttonExit);
        authorsController = new AuthorsController(game, buttonStartExit);
        batch = new SpriteBatch();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (Gdx.input.isTouched()) {

            authorsController.move(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
        }

        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            game.setScreen(new MenuScreen(game));
        }

        camera.update();
        batch.begin();
        bgSprite.draw(batch);
        batch.setProjectionMatrix(camera.combined);
        authorsController.render(batch);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();

        authorsController.dispose();
    }
}
