package ru.topteam.bigame.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import java.lang.reflect.GenericDeclaration;

import ru.topteam.bigame.controllers.GameWinController;
import ru.topteam.bigame.controllers.QuestController;
import ru.topteam.bigame.ui.Button;

/**
 * @author egor ivanchin
 */
public class GameWinScreen implements Screen {
    private SpriteBatch batch;
    private Game game;
    private GameWinController gameWinController;
    private Sprite bgSprite;
    private Music winMusic;
    private int widthTexture = 512;
    private int heightTexture = 135;

    public GameWinScreen(Game game) {
        this.game = game;
    }

    @Override
    public void show() {
        bgSprite = new Sprite(new Texture(Gdx.files.internal("youWinScreen.png")));
        bgSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        Texture buttonImage = new Texture(Gdx.files.internal("back_to_menu_button.png"));
        Rectangle goMenuHitbox = new Rectangle(Gdx.graphics.getWidth() / 2 - widthTexture / 2, Gdx.graphics.getHeight() / 2 - heightTexture / 2 - 240, widthTexture, heightTexture);
        Button goMenuButton = new Button(buttonImage, goMenuHitbox);
        gameWinController = new GameWinController(game, goMenuButton);
        batch = new SpriteBatch();
        winMusic = Gdx.audio.newMusic(Gdx.files.internal("music/win.mp3"));
        winMusic.setVolume(10);
        winMusic.play();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (Gdx.input.isTouched()) {
            gameWinController.move(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
        }

        batch.begin();
        bgSprite.draw(batch);
        gameWinController.render(batch);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
        winMusic.pause();
    }

    @Override
    public void resume() {
        winMusic.play();
    }

    @Override
    public void hide() {
        winMusic.pause();
    }

    @Override
    public void dispose() {
        batch.dispose();
        gameWinController.dispose();
        winMusic.dispose();
    }
}
