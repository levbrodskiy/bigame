package ru.topteam.bigame.main;

import com.badlogic.gdx.Game;

import ru.topteam.bigame.screens.MenuScreen;

/**
 * @author Maxim Lebedev
 */
public class StartScreen extends Game {

    @Override
    public void create() {
        setScreen(new MenuScreen(this));
    }
}
